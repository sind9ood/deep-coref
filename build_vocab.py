import utils
import sys
import numpy

reload(sys)
sys.setdefaultencoding('utf-8')


def build_vocab_for_coref():
    """
    Build "form" file for java application from python "vocabulary.pkl"
    """
    vocab = utils.load_pickle("data/features/mention_data/vocabulary.pkl")
    new_vocab = {}
    for key, value in vocab.items():
        new_vocab[value] = key

    new_vocab[0] = "<PAD>"

    wfile = open("data/form.txt", "wb")
    wfile.write("form\n")

    for i, token in new_vocab.items():
        wfile.write(token + " " + str(i) + "\n")


def build_wv_for_feature_coref():
    """
    Convert word vectors in numpy format into text form for java application
    """
    wv_file = numpy.load("data/features/mention_data/word_vectors.npy")
    out_file = open("data/wv.txt", "wb")
    for line in wv_file:
        for l, value in enumerate(line):
            out_file.write(str(value))
            if l != len(line) - 1: out_file.write(" ")
        out_file.write('\n');



if __name__ == '__main__':
    # build_vocab_for_coref()
    build_wv_for_feature_coref()
