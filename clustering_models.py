import directories
import model_properties
from custom_neural_implementations import *

import numpy as np
import tensorflow as tf

class ClusteringModel(object):
    def __init__(self, session, model_props, is_training):
        self._sess = session
        self.is_training = is_training

        # TODO: Handle static models
        self._inference(model_props)
        session.run(tf.initialize_variables([var for var in tf.all_variables() if "cluster" in var.name]))
        self.saver = tf.train.Saver([var for var in tf.trainable_variables() if "cluster" in var.name])
        dynamic_ana, dynamic_pair, self.word_vectors = self.get_weights(model_props)

        if not model_props.randomize_weights:
            self.set_weights(session, dynamic_ana + dynamic_pair)

    def anaphoricity_weights(self):
        weights = tf.trainable_variables()
        return [
               np.vstack([np.asarray(weights[i].eval()) for i in range(1, 7, 2)]),
               np.sum(np.vstack([np.asarray(weights[i].eval()) for i in range(2, 8, 2)]), axis=0),
           ] + [np.asarray(w.eval()) for w in weights[7:15]]


    def pair_weights(self):
        weights = tf.trainable_variables()
        return [
                   np.vstack([np.asarray(weights[i].eval()) for i in range(15, 25, 2)]),
                   np.sum(np.vstack([np.asarray(weights[i].eval()) for i in range(16, 26, 2)]), axis=0),
               ] + [np.asarray(w.eval()) for w in weights[25:33]]


    def get_weights(self, model_props):
        w_ana = self.anaphoricity_weights()
        w_pair = self.pair_weights()
        w_vectors = np.asarray(tf.trainable_variables()[0].eval())

        w_ana[-2] *= -0.3
        w_ana[-1] -= 1
        dynamic_pair = w_pair[:-4]

        Wscore, bscore = w_pair[-4], w_pair[-3]
        Wscale, bscale = w_pair[-2], w_pair[-1]
        Wscore = np.vstack([Wscore for _ in range(2 if model_props.pooling == 'maxavg' else 1)]) + \
                 (0.1 * (np.random.random((2 * Wscore.shape[0], Wscore.shape[1])) - 0.5))
        Wscore[Wscore.shape[0]/2, :] *= 0.15
        dynamic_pair += [Wscore, bscore, Wscale, bscale]
        return w_ana, dynamic_pair, w_vectors


    def set_weights(self, session, weights):
        clusterer_vars = [var for var in tf.trainable_variables() if "cluster" in var.name]
        for i, weight in enumerate(weights):
            session.run(tf.assign(clusterer_vars[i], weight))


    def clusterer_layer(self, input, keep_prob):
        # TODO: Only covers fixed 3-layer dynamic layers
        # TODO: Need to be updated if 1 or 2 layer layers are used
        if self.is_training and keep_prob < 1.0:
            in_drop = tf.nn.dropout(input, keep_prob)
        else:
            in_drop = input

        with tf.variable_scope("h1drop"):
            h1drop = fully_connected(in_drop, in_drop.get_shape()[1], 1000, is_training=self.is_training, keep_prob=keep_prob)
        with tf.variable_scope("h2drop"):
            h2drop = fully_connected(h1drop, 1000, 500, is_training=self.is_training, keep_prob=keep_prob)
        with tf.variable_scope("h3drop"):
            h3drop = fully_connected(h2drop, 500, 500, is_training=self.is_training, keep_prob=keep_prob)
        return h3drop


    def score_layer(self, input):
        with tf.variable_scope("score"):
            init_score = fully_connected(input, input.get_shape()[1], 1)
        with tf.variable_scope("scale"):
            return tf.reshape(fully_connected(init_score, 1, 1), [-1])


    def _inference(self, model_props):
        keep_prob = 1.0 - model_props.dropout
        self.mention_features = tf.placeholder(tf.float32, [None, model_props.anaphoricity_input_size], name="mention_features")
        self.pair_features = tf.placeholder(tf.float32, [None, model_props.pair_input_size], name="pair_features")
        self.starts = tf.placeholder(tf.int32, [None], name="starts")
        self.ends = tf.placeholder(tf.int32, [None], name="ends")
        self.costs = tf.placeholder(tf.float32, [None], name="costs")

        with tf.variable_scope("anaphoricity"):
            anaphoricity_repr = self.clusterer_layer(self.mention_features, keep_prob)
            anaphoricity_score = self.score_layer(anaphoricity_repr)

        with tf.variable_scope("pair"):
            pair_repr = self.clusterer_layer(self.pair_features, keep_prob)
            cluster_reprs = merge_max_mean(pair_repr, self.starts, self.ends)
            merge_score = self.score_layer(cluster_reprs)

        self.action_scores = tf.concat(0, [merge_score, anaphoricity_score])
        self.loss = risk(self.costs, self.action_scores) if model_props.risk_objective \
            else max_margin(self.costs, self.action_scores)

        self.train_op = tf.train.AdamOptimizer(model_props.learning_rate, 0.9, 0.99, 1e-6).minimize(self.loss)
        print "Compiling clustering model"


    def train_on_batch(self, X):
        feed_dict = {self.pair_features: X['pair_features'], self.mention_features: X['mention_features'],
                     self.costs: np.reshape(X['costs'], [-1]), self.starts: np.reshape(X['starts'], [-1]),
                     self.ends: np.reshape(X['ends'], [-1])}

        loss, _ = self._sess.run([self.loss, self.train_op], feed_dict=feed_dict)
        return loss


    def predict_on_batch(self, X):
        feed_dict = {self.pair_features: X['pair_features'], self.mention_features: X['mention_features'],
                     self.starts: np.reshape(X['starts'], [-1]), self.ends: np.reshape(X['ends'], [-1])}

        action_score = self._sess.run([self.action_scores], feed_dict=feed_dict)
        return action_score