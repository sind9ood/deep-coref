import h5py
import cPickle
import os

def import_weights(filepath):
    """
    Import hdf5 format weights from theano version
    """
    f = h5py.File(filepath)
    g = f['graph']
    weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]
    return weights


def save_weights(weights, export_dir="export"):
    """
    Save dictinary form of weights using pickle
    """
    weights_dic = {}
    for weight in weights:
        weights_dic[weight.name.replace('/graph/','')] = weight.value.tolist();
    cPickle.dump(weights_dic, open(os.path.join(export_dir, "weights.pkl"), "wb"))


if __name__ == '__main__':
    # filepath = "data/models/ranking/best_weights.hdf5"
    filepath = "data/best_weights.hdf5"

    imported_weights = import_weights(filepath)
    save_weights(imported_weights)