import tensorflow as tf


def get_activation_func(act_str):
    if act_str == 'relu':
        return tf.nn.relu
    elif act_str == 'sigmoid':
        return tf.sigmoid
    elif act_str == 'tanh':
        return tf.tanh
    else:
        return tf.nn.relu


def fully_connected(X, s1, s2, keep_prob=None, is_training=False, activation=None, force_init=False):
    if force_init:
        W = tf.get_variable(initializer=tf.constant([[1.0]]), name="W")
        b = tf.get_variable(initializer=tf.constant([0.0]), name="b")
    else:
        W = tf.get_variable(initializer=tf.contrib.layers.xavier_initializer(uniform=True), name="W", shape=[s1, s2])
        b = tf.get_variable(name="b", shape=[s2])

    output = tf.nn.xw_plus_b(X, W, b)

    if activation is not None:
        # output = activation(output)
        output = tf.nn.relu(output)

    if is_training and keep_prob is not None:
        output = tf.nn.dropout(output, keep_prob)

    return output


def gather_and_FF(src, idx, out_size, keep_prob=None, is_training=True):
    gathered = tf.gather(src, idx)

    if is_training and keep_prob is not None:
        gathered = tf.nn.dropout(gathered, keep_prob)

    return fully_connected(gathered, gathered.get_shape()[1], out_size, is_training=is_training)


def merge_max_margin(costs, scores, starts, ends):

    def gather_max(r):
        cst = tf.gather(costs, tf.range(r[0], r[1]))
        scr = tf.gather(scores, tf.range(r[0], r[1]))
        ind = tf.squeeze((tf.where(tf.equal(cst, 0))))
        score_ind = tf.gather(scr, ind)
        return tf.reduce_max(cst * (3.0 + scr - tf.reduce_max(score_ind)))

    return tf.reshape(tf.map_fn(gather_max, (starts, ends), dtype=tf.float32), [-1, 1])


def merge_max_margin_2(costs, scores, starts, ends):

    def gather_max(r):
        cst = tf.gather(costs, tf.range(r[0], r[1]))
        scr = tf.gather(scores, tf.range(r[0], r[1]))
        ind = tf.where(tf.equal(cst, 0))[:,0]
        score_ind = tf.gather(scr, ind)
        return tf.reduce_max(cst * (3.0 + scr - tf.reduce_max(score_ind)))

    return tf.reshape(tf.map_fn(gather_max, (starts, ends), dtype=tf.float32), [-1, 1])


def merge_risk(scores, costs, starts, ends):

    def gather_max_mean(r):
        gather_score = tf.gather(scores, tf.range(r[0], r[1]))
        gather_cost = tf.gather(costs, tf.range(r[0], r[1]))
        return tf.reduce_sum(gather_cost * tf.nn.softmax(gather_score))

    return tf.map_fn(gather_max_mean, (starts, ends), dtype=tf.float32)


def merge_max_mean(pair, starts, ends):

    def gather_max_mean(r):
        gathered = tf.gather(pair, tf.range(r[0], r[1]))
        max_gather = tf.reduce_max(gathered, reduction_indices=0)
        mean_gather = tf.reduce_mean(gathered, reduction_indices=0)
        return tf.concat(0, [max_gather, mean_gather])

    return tf.map_fn(gather_max_mean, (starts, ends), dtype=tf.float32)


def get_summed_xent(scale_factor, y_true, y_pred):
    epsilon = 1e-7
    y_pred = tf.clip_by_value(y_pred, epsilon, 1.0 - epsilon)
    y_true = tf.to_float(y_true)
    bce = -tf.reduce_sum(y_true * tf.log(y_pred) + (1.0-y_true) * tf.log(1.0-y_pred))

    return bce / scale_factor


def max_margin(costs, scores):
    ind = tf.squeeze((tf.where(tf.equal(costs, 0))))
    return tf.reduce_max(costs * ( 2.0 + scores - tf.reduce_max(scores[ind])))


def risk(costs, scores):
    e_x = tf.exp(scores - tf.reduce_max(scores, reduction_indices=0, keep_dims=True))
    e_x = e_x / tf.reduce_sum(e_x, reduction_indices=0, keep_dims=True)
    return tf.reduce_sum(costs * e_x)