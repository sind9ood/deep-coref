import os
import re

import utils
import copy
import collections

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

def docs(dataset_name, mention_dir):
    p = utils.Progbar(target=(utils.lines_in_file(mention_dir + dataset_name)))
    for i, d in enumerate(utils.load_json_lines(mention_dir + dataset_name)):
        p.update(i + 1)
        yield d


def extract_mention_labels(mention, labels):
    sent = mention["sentence"]

    start_index = mention["start_index"]
    end_index = mention["end_index"]
    mention_type = mention["mention_type"]

    for i, token in enumerate(sent):

        if i == start_index and i+1 == end_index:
            if labels and labels[i]!=u"O":
                labels[i] += u"|S_" + mention_type
            else: labels[i] = u"S_" + mention_type
        elif i == start_index:
            if labels and labels[i]!=u"O":
                labels[i] += u"|B_" + mention_type
            else: labels[i] = u"B_" + mention_type
        elif i+1 == end_index:
            if labels and labels[i]!=u"O":
                labels[i] += u"|E_" + mention_type
            else: labels[i] = u"E_" + mention_type
        elif i > start_index and i+1 < end_index:
            if labels and labels[i] != u"O":
                continue
            else: labels[i] = u"O"
        else:
            if labels and labels[i] != u"O":
                continue
            else: labels[i] = u"O"

    return labels


def write_md_data(label_docs, fix_data, dataset_name, target_dir):

    wfile = open(target_dir + dataset_name + ".txt", "w")

    for did, d in enumerate(label_docs):
        for sid, sent in enumerate(d.values()):

            # Check if sentence is in "fix data" => replace labels if any
            if (str(did), str(sid)) in fix_data:
                sent["labels"] = fix_data[(str(did), str(sid))][1]

            stack = []
            for i in range(len(sent["tokens"])):
                labels = sent["labels"][i].split("|")

                # result labels
                sorted_labels = []

                b_labels = [l for l in labels if l.startswith("B_")]
                e_labels = [l for l in labels if l.startswith("E_")]
                s_labels = [l for l in labels if l.startswith("S_")]

                # Build reduced labels for checking stacks
                reduced_e_labels = [l.replace("E_", "") for l in labels if l.startswith("E_")]

                # Treat S labels first
                for s_l in s_labels:
                    sorted_labels.append(s_l.replace("S_", "B_"))
                    sorted_labels.append(s_l.replace("S_", "E_"))

                # Treat E labels second : Look before to see if they already had same "B_" labels
                while True:
                    e_labels_set = collections.Counter(reduced_e_labels)

                    if stack:
                        temp_prev_set = collections.Counter([stack[-1]])
                        e_intersect = list((e_labels_set & temp_prev_set).elements())

                        if e_intersect:
                            r_index = reduced_e_labels.index(e_intersect[0])
                            if isinstance(r_index, list):
                                r_index = r_index[0]

                            sorted_labels.append(e_labels[r_index])
                            e_labels.pop(r_index)
                            reduced_e_labels.pop(r_index)
                            stack.pop()
                        else: break
                    else: break

                # Treat B labels
                if len(b_labels) > 1:
                    # Get all possible sequences
                    seqs = list(get_all_sequences([], b_labels))

                    # Check validitiy of all sequences and get valid one
                    valid_seq = None
                    for seq in seqs:
                        if get_valid(seq, e_labels, sent["labels"], i):
                            valid_seq = seq
                            break

                    try:
                        assert valid_seq != None, "Invalid sequence"
                    except:
                        print_exception(did, sid, sent["tokens"], sent["labels"])

                    valid_seq_list = valid_seq.split(' ')
                    for v_l in valid_seq_list: stack.append(v_l.replace("B_", ""))
                    sorted_labels = valid_seq_list

                # Skip getting valid sequence if number of b_labels == 1
                elif len(b_labels) == 1:
                    sorted_labels.append(b_labels[0])
                    stack.append(b_labels[0].replace("B_", ""))

                # Treat E labels
                if e_labels:
                    e_labels_set = collections.Counter(e_labels)
                    # Iterate until we consume all E_ labels
                    while e_labels_set:
                        stack_set = collections.Counter("E_" + stack.pop())
                        intersect = list((e_labels_set & stack_set).elements())

                        try:
                            assert len(intersect) > 0, "Invalid end label"
                        except:
                            print_exception(did, sid, sent["tokens"], sent["labels"])

                        sorted_labels.append(intersect[0])
                        e_labels_set = e_labels_set - stack_set
                else:
                    if not sorted_labels:
                        sorted_labels = ["O"]

                wfile.write(sent["tags"][i] + "\t" + "|".join(sorted_labels))
                wfile.write("\n")
            wfile.write("\n")
        wfile.write("EOD\n")


def build_data_for_mention_detection(conll_data, mention_dir, target_dir):

    for dataset_name in ["train", "dev", "test"]:

        print "Building mention detection label data for ", dataset_name

        label_docs = []
        conll = conll_data[dataset_name]

        for doc_id, d in enumerate(docs(dataset_name, mention_dir)):
            conll_doc = conll[doc_id]
            sents_map = {}

            for i, sentence in enumerate(d["sentences"]):
                sents_map[i] = {}
                sents_map[i]["tokens"] = sentence
                sents_map[i]["tags"] = conll_doc[i]
                sents_map[i]["labels"] = [u"O"]*len(sentence)

            for mention in d["mentions"].values():
                sents_map[mention["sent_num"]]["labels"] = \
                    extract_mention_labels(mention, sents_map[mention["sent_num"]]["labels"])

            label_docs.append(sents_map)

        # Refine labels to avoid ill-formed strcture
        refine_labels(label_docs)

        # Fix wrong labels remaining manually
        fix_data = read_fix_data(dataset_name)

        # Write labels : Re-ordering labels is necessary
        write_md_data(label_docs, fix_data, dataset_name, target_dir)


def print_exception(did, sid, tokens, labels):
    print "---  " + str(did) + ":" + str(sid)
    for i in range(len(tokens)):
        print tokens[i] + "\t" + labels[i]


def update_position(b_prop, e_prop, b_other, e_other, labels, m_type):

    if b_other > b_prop and b_other < e_prop or e_other < e_prop and e_other > b_prop:

        if b_other > b_prop and b_other < e_prop:
            e_other = get_next_target_index(labels, b_other+1, m_type)
        elif e_other < e_prop and e_other > b_prop:
            b_other = get_prev_target_index(labels, e_other-1, m_type)

        if only_one_overlapping_mention_tag_in_the_range(labels, b_prop, e_prop) or e_prop - b_prop <= 8:

            # Move the pointer of "B_Label" to the left
            if b_other > b_prop and b_other < e_prop and e_other > e_prop:
                if u"B_" + m_type in labels[b_other]:
                    delete_LBL(labels, b_other, u"B_" + m_type)
                    labels[b_prop] += (u"|B_" + m_type)
                    return True

            # Move the pointer of "E_Label" to the right
            elif e_other > b_prop and e_other < e_prop and b_other < b_prop:
                if u"E_" + m_type in labels[e_other]:
                    delete_LBL(labels, e_other, u"E_" + m_type)
                    labels[e_prop] += (u"|E_" + m_type)
                    return True

    return False


def refine_labels(label_docs):
    '''
    Rule-based label refinement : to refine ill-formed mention ranges
    '''
    for did, d in enumerate(label_docs):
        for sid, sent in enumerate(d.values()):
            prop_stack = []
            b_nom_index, e_nom_index, b_list_index, e_list_index = -1, -1, -1, -1

            i = 0
            updated = False
            while i in range(len(sent["tokens"])):
                labels = sent["labels"][i].split("|")

                lable_counter = collections.Counter(labels)
                while lable_counter:
                    '''
                    Main idea here is to check if another type mention is inside of "PROPER" type mention
                    '''
                    if u"E_PROPER" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"E_PROPER"])
                        b_prop = prop_stack.pop()
                        e_prop = i

                        updated_1 = update_position(b_prop, e_prop, b_nom_index, e_nom_index, sent["labels"], u"NOMINAL")
                        updated_2 = update_position(b_prop, e_prop, b_list_index, e_list_index, sent["labels"], u"LIST")
                        updated = updated_1 or updated_2

                    elif u"B_PROPER" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"B_PROPER"])
                        prop_stack.append(i)

                    elif u"B_NOMINAL" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"B_NOMINAL"])
                        b_nom_index = i

                    elif u"E_NOMINAL" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"E_NOMINAL"])
                        e_nom_index = i

                    elif u"B_LIST" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"B_LIST"])
                        b_list_index = i

                    elif u"E_LIST" in lable_counter:
                        lable_counter = lable_counter - collections.Counter([u"E_LIST"])
                        e_list_index = i

                    else: break

                if updated:
                    updated = False
                    i = 0
                else: i += 1


def get_next_target_index(labels, i, target):
    offset = 0
    for k in range(i, len(labels)):
        if "E_"+target in labels[k]:
            if offset == 0:
                return k
            else: offset -= 1
        elif "B_"+target in labels[k]:
            offset += 1

    return len(labels)


def get_prev_target_index(labels, i, target):
    offset = 0
    for k in range(i, -1, -1):
        if "B_"+target in labels[k]:
            if offset == 0:
                return k
            else: offset -= 1
        elif "E_"+target in labels[k]:
            offset += 1

    return len(labels)


def only_one_overlapping_mention_tag_in_the_range(labels, s, e):
    count = 0
    stack = []
    for i in range(s+1, e):
        if labels[i] != u'O':
            if stack and labels[i].startswith("E_") and stack[-1].startswith("B_") and labels[i].replace("E_", "") == stack[-1].replace("B_", ""):
                stack.pop()
                count -= 1
            else:
                stack.append(labels[i])
                count += 1
    if count < 2:
        return True
    return False


def delete_LBL(labels, index, prop_label):
    temp_l = labels[index].split("|")
    temp_l.remove(prop_label)
    if temp_l:
        labels[index] = '|'.join(temp_l)
    else: labels[index] = u"O"


def get_all_sequences(result, orig):
    if len(orig) == 0:
        return [' '.join(result)]

    sequences = set()
    for element in orig:
        new_orig = copy.copy(orig)
        new_orig.remove(element)
        new_res = copy.copy(result)
        new_res.append(element)
        intermediate = get_all_sequences(new_res, new_orig)
        for obj in intermediate:
            sequences.add(obj)

    return sequences


def get_valid(seq, given_e, labels, index):
    '''
    Simulate to check if given sequence is valid, i.e. well-formed
    '''
    seq_labels = seq.split(" ")
    stack = [l.replace("B_", "E_") for l in seq_labels]

    for i in range(index, len(labels)):
        ll = labels[i].split('|')
        b_labels = [l.replace("B_", "E_") for l in ll if l.startswith("B_")]
        e_labels = [l for l in ll if l.startswith("E_")]

        # Check e label first in stack
        if i == index:
            e_labels = given_e
        else:
            e_labels_set = collections.Counter(e_labels)

            while e_labels_set:

                if not stack: return True

                if isinstance(stack[-1], list):
                    stack_element = stack[-1]
                else: stack_element = [stack[-1]]

                stack_set = collections.Counter(stack_element)
                intersect = list((e_labels_set & stack_set).elements())
                if intersect:
                    stack.pop()
                    diff = list((stack_set - e_labels_set).elements())
                    if diff: stack.append(list(diff))
                    e_labels_set = e_labels_set - stack_set
                    e_labels = list(e_labels_set.elements())
                else:
                    break

        # Else put b_labels
        if i > index and len(b_labels) > 0:
            stack.append(b_labels)

        if len(e_labels) > 0:

            # Traverse through all e_labels (stack element can be multiple)
            # All e_labels should be consumed, but without order
            e_labels_set = collections.Counter(e_labels)

            while e_labels_set:
                if not stack:
                    return True

                if isinstance(stack[-1], list):
                    stack_element = stack[-1]
                else: stack_element = [stack[-1]]

                stack_set = collections.Counter(stack_element)
                intersect = list((e_labels_set & stack_set).elements())
                if not intersect:
                    return False
                else:
                    stack.pop()
                    diff = list((stack_set - e_labels_set).elements())
                    if diff: stack.append(list(diff))
                    e_labels_set = e_labels_set - stack_set

        if len(stack) == 0:
            return True

    return False


def read_conll_data(conll_dir):

    conll_data = {}

    for dataset_name in ["train", "dev", "test"]:
        data_dir = conll_dir + "flat_" + dataset_name + "_2012/"
        dirs = os.listdir(data_dir)
        docs, doc = [], []

        for file in [os.path.join(data_dir, f) for f in dirs]:
            for line in open(file):

                if "#begin document" in line:
                    doc, unit_token, ner_stack = [], [], []

                elif "#end document" in line:
                    if doc:
                        docs.append(doc)

                else:
                    if line == "\n":
                        if unit_token is not "":
                            doc.append(unit_token)
                            unit_token, ner_stack = [], []
                    else:
                        elements = re.split("\s\s+", line.rstrip("\n"))
                        ner_tag = to_BEIO(elements[10], ner_stack)
                        tags = ' '.join([elements[3], elements[4], ner_tag])
                        unit_token.append(tags)

        conll_data[dataset_name] = docs

    return conll_data


def to_BEIO(str, stack):
    if str.startswith("(") and str.endswith(")"):
        ner_type = str.replace(")","").replace("(","").replace("*","")
        new_str = "B_" + ner_type + "|" + "E_" + ner_type
    elif str.startswith("("):
        ner_type = str.replace("(", "").replace("*","")
        new_str = "B_" + ner_type
        stack.append(ner_type)
    elif str.endswith(")"):
        ner_type = stack.pop()
        new_str = "E_" + ner_type
    elif len(stack) > 0 and "*" in str:
        new_str = "I_" + stack[-1]
    else:
        new_str = "O"

    return new_str


def read_fix_data(dataset_name):

    if os.path.isfile("md_"+dataset_name+"_fix.txt"):
        fix_map = {}

        fix_file = open("md_"+dataset_name+"_fix.txt")
        did, sid = -1, -1
        tokens, labels = [], []
        for line in fix_file:
            if line.startswith("---"):
                if did != -1 and sid != -1:
                    fix_map[(did, sid)] = [tokens, labels]
                tokens = []
                labels = []
                elements = re.split("\s\s+", line.rstrip("\n"))
                did, sid = elements[1].split(":")
            else:
                token, label = line.rstrip("\n").split('\t')
                tokens.append(token)
                labels.append(label)

        if did != -1 and sid != -1:
            fix_map[(did, sid)] = [tokens, labels]

        return fix_map
    else: return {}


if __name__ == '__main__':

    conll_dir = "../conll_data/"
    mention_dir = "../data/data_raw/"
    target_dir = "data/"

    # read conll data for tokens, POS, ner
    conll_data = read_conll_data(conll_dir)

    # read label data
    build_data_for_mention_detection(conll_data, mention_dir, target_dir)
