import numpy as np
import directories
import timer
import os
import h5py

from custom_neural_implementations import *

class PairwiseModel(object):
    """
    Coreference network for training.
    """
    def __init__(self, session, dataset, model_props, vectors, is_training=True, representation=False):
        self._sess = session
        self.is_training = is_training

        # self.input_sizes = {k: v.shape[1] for k, v in next(X for X in dataset).iteritems() if v.ndim == 2}
        # [Note] Use fixed dim for inference
        self.mention_feat_sz = 24
        self.pair_feat_sz = 70
        self.spans_sz = 250
        self.words_sz = 8

        self.model_props = model_props
        self.representation = representation
        self.act_func = get_activation_func(self.model_props.activation)

        self._build_inputs()

        timer.start("compile")
        self._inference(vectors) # (batch_size, vocab_size)

        if model_props.ranking:
            n = dataset.scale_factor * (0.1 if model_props.reinforce else 1.0) if dataset is not None else 1.0
            self.loss = (tf.reduce_sum(self._y)) / n
        else:
            if not model_props.anaphoricity_only:
                scale_factor = dataset.scale_factor if dataset is not None else 1.0
                print ("scale factor : %f" % scale_factor)
                self.loss = get_summed_xent(scale_factor, self.gold_y, self._y)
                self.loss += tf.reduce_sum([tf.nn.l2_loss(v) for v in tf.trainable_variables() if "W" in v.name and "anaphoricity" not in v.name]) \
                               *model_props.regularization
            if model_props.anaphoricity:
                anaphoricity_scale_factor = dataset.anaphoricity_scale_factor if dataset is not None else 1.0
                print ("anaphoricity scale factor : %f" % anaphoricity_scale_factor)
                self.loss += get_summed_xent(anaphoricity_scale_factor, self.gold_anaphoricities, self.anaphoricities)
                self.loss += tf.reduce_sum([tf.nn.l2_loss(v) for v in tf.trainable_variables() if "W" in v.name and "anaphoricity" in v.name]) \
                                *model_props.regularization

        self.train_op = tf.train.AdamOptimizer(model_props.lr*0.1).minimize(self.loss)
        timer.stop("compile")

        init_op = tf.global_variables_initializer()

        self.saver = tf.train.Saver([v for v in tf.trainable_variables() if v.name.startswith(model_props.mode)])
        if model_props.load_weights_from is not None:
            session.run(init_op)
            if model_props.model_from_theano:
                self.load_weights_from_theano(session)
            else:
                target_path = os.path.join(directories.MODELS+model_props.load_weights_from, model_props.weights_file+".ckpt")
                self.old_saver = tf.train.import_meta_graph(os.path.join(target_path+'.meta'))
                self.old_saver.restore(session, target_path)
                print "loaded"
                src_vars = [v for v in tf.trainable_variables() if v.name.startswith(model_props.load_weights_from)]
                dst_vars = [v for v in tf.trainable_variables() if v.name.startswith(model_props.mode)]
                for i, tf_var in enumerate(dst_vars):
                    session.run(tf_var.assign(src_vars[i]))
        else:
            if is_training:
                session.run(init_op)
                session.run(self.lookup.assign(vectors))


    def _build_inputs(self):
        self.anaphors = tf.placeholder(tf.int64, [None], name="anaphors")
        self.words = tf.placeholder(tf.int32, [None, self.words_sz], name="words")
        self.antecedents = tf.placeholder(tf.int64, [None], name="antecedents")
        self.pair_antecedents = tf.placeholder(tf.int32, [None], name="pair_antecedents")
        self.pair_anaphors = tf.placeholder(tf.int32, [None], name="pair_anaphors")
        self.pair_features = tf.placeholder(tf.float32, [None, self.pair_feat_sz], name="pair_features")

        if self.model_props.use_spans:
            self.spans = tf.placeholder(tf.float32, [None, self.spans_sz], name="spans")

        if self.model_props.anaphoricity:
            self.mention_features = tf.placeholder(tf.float32, [None, self.mention_feat_sz], name="mention_features")

        if self.model_props.top_pairs:
            self.starts = tf.placeholder(tf.int32, [None], name="starts")
            self.ends = tf.placeholder(tf.int32, [None], name="ends")
            self.score_inds = tf.placeholder(tf.int32, [None], name="score_inds")
        elif self.model_props.ranking:
            self.reindex = tf.placeholder(tf.int32, [None], name="reindex")
            self.starts = tf.placeholder(tf.int32, [None], name="starts")
            self.ends = tf.placeholder(tf.int32, [None], name="ends")
            self.costs = tf.placeholder(tf.float32, [None], name="costs")
            self.ids = tf.placeholder(tf.int32, [None, 2], name="ids")

        self.gold_anaphoricities = tf.placeholder(tf.float32, [None], name="gold_anaphoricities")
        self.gold_y = tf.placeholder(tf.int32, [None], name="gold_y")


    def get_top_layers(self, input):
        layer_size = self.model_props.layer_sizes[0]
        assert input.get_shape()[1] == layer_size

        activation = self.act_func(input)
        input = activation

        for i in range(len(self.model_props.layer_sizes) - 1):
            with tf.variable_scope("layer_"+str(i+1)):
                if self.is_training and self.model_props.dropout > 0:
                    dropped = tf.nn.dropout(input, 1.0-self.model_props.dropout)
                else:
                    dropped = input

                output = fully_connected(dropped, dropped.get_shape()[1], self.model_props.layer_sizes[i + 1],
                                         is_training=self.is_training, activation=self.act_func)
            input = output

        if not self.representation:
            with tf.variable_scope("score"):
                if self.is_training and self.model_props.dropout > 0:
                    score_drop = tf.nn.dropout(input, 1.0-self.model_props.dropout)
                else:
                    score_drop = input

                with tf.variable_scope("layer_1"):
                    ff_1 = fully_connected(score_drop, score_drop.get_shape()[1], 1, is_training=self.is_training)
                with tf.variable_scope("layer_2"):
                    ff_2 = fully_connected(ff_1, 1, 1, is_training=self.is_training, force_init=True)
                if not self.model_props.ranking:
                    ff_2 = tf.nn.sigmoid(ff_2)

        return tf.reshape(ff_2, [-1])


    def get_anaphoricity_reprs(self):
        layer_size = self.model_props.layer_sizes[0]

        if self.model_props.use_spans:
            with tf.variable_scope("spans"):
                anaphoricity_spans = gather_and_FF(self.spans, self.anaphors, layer_size)
        with tf.variable_scope("anaphoricity_emb"):
            anaphoricity_emb = gather_and_FF(self.flattend_embeddings, self.anaphors, layer_size,
                                             keep_prob=1.0-self.model_props.dropout, is_training=self.is_training)
        with tf.variable_scope("anaphoricity_feat"):
            anaphoricity_feat = gather_and_FF(self.mention_features, self.anaphors, layer_size)
        return tf.reduce_sum([anaphoricity_spans, anaphoricity_emb, anaphoricity_feat], reduction_indices=0)


    def get_mention_reprs(self, base_input, pair_input):
        layer_size = self.model_props.layer_sizes[0]

        if self.model_props.use_spans:
            with tf.variable_scope("spans"):
                mention_spans = gather_and_FF(self.spans, base_input, layer_size)
                pair_mention_spans = tf.gather(mention_spans, pair_input)

        pair_flattened_emb = tf.gather(self.flattend_embeddings, base_input)
        with tf.variable_scope("mention_emb"):
            pair_mention_embs = gather_and_FF(pair_flattened_emb, pair_input, layer_size,
                                              keep_prob=1.0-self.model_props.dropout, is_training=self.is_training)
        if self.model_props.use_spans:
            return tf.reduce_sum([pair_mention_spans, pair_mention_embs], 0)
        else:
            return pair_mention_embs


    def _inference(self, vectors):
        with tf.variable_scope("embedding"):
            self.lookup = tf.get_variable(name="lookup", shape=[vectors.shape[0], vectors.shape[1]],
                                          trainable=not self.model_props.freeze_embeddings)
        word_embeddings = tf.nn.embedding_lookup(self.lookup, self.words)
        w_shape = word_embeddings.get_shape().as_list()
        self.flattend_embeddings = tf.reshape(word_embeddings, shape=[-1, np.prod(w_shape[1:])])

        if self.model_props.anaphoricity:
            with tf.variable_scope("anaphoricity"):
                anaphoricity_reprs = self.get_anaphoricity_reprs()
                top_anaphoricity = self.get_top_layers(anaphoricity_reprs)

            if not self.model_props.ranking:
                self.anaphoricities = top_anaphoricity

        if self.model_props.anaphoricity_only:
            return

        with tf.variable_scope("mention") as mention_scope:
            pair_antecedent_reprs = self.get_mention_reprs(self.antecedents, self.pair_antecedents)
            mention_scope.reuse_variables();
            pair_anaphor_reprs = self.get_mention_reprs(self.anaphors, self.pair_anaphors)

        with tf.variable_scope("pair_feature"):
            pair_feature_reprs = fully_connected(self.pair_features, self.pair_features.get_shape()[1],
                                      self.model_props.layer_sizes[0], is_training=self.is_training)

            top = self.get_top_layers(tf.reduce_sum([pair_antecedent_reprs, pair_anaphor_reprs, pair_feature_reprs],
                                                    reduction_indices=0))

        if self.model_props.top_pairs:
            reordered = tf.gather(top, self.score_inds)

            def gather_max(r):
                return tf.reduce_max(tf.gather(reordered, tf.range(r[0], r[1])))
            self._y = tf.map_fn(gather_max, (self.starts, self.ends), dtype=tf.float32)

        elif self.model_props.ranking:
            if self.model_props.anaphoricity:
                ana_score_W = tf.Variable(tf.constant(-0.3, shape=[1, 1]), name="ana_score_W", trainable=False)
                ana_score_b = tf.Variable(tf.constant(-1.0, shape=[1]), name="ana_score_b", trainable=False)
                anaphoricity_scores = tf.nn.xw_plus_b(tf.expand_dims(top_anaphoricity, 1), ana_score_W, ana_score_b)

                concatenated_scores = tf.concat([top, tf.reshape(anaphoricity_scores, [-1])], 0)
                scores_reindexed = tf.gather(concatenated_scores, self.reindex)
            else:
                scores_reindexed = tf.gather(top, self.reindex)

            if self.model_props.reinforce:
                anaphor_losses = merge_risk(self.costs, scores_reindexed, self.starts, self.ends)
            else:
                anaphor_losses = merge_max_margin(self.costs, scores_reindexed, self.starts, self.ends)

            self._y = anaphor_losses
            self._z = scores_reindexed
        else:
            self._y = top


    def get_pairs(self):
        """
        Replaces "update_doc" function in pairwirse_learning that suggests final clustering pairs
        """
        def my_gather(r):
            scr = tf.gather(self._z, tf.range(r[0], r[1]))
            max_ind = tf.arg_max(scr, 0)
            id_index = tf.add(r[0],tf.cast(max_ind, dtype=tf.int32))
            return tf.gather_nd(self.ids, [id_index])

        whole_links = tf.map_fn(my_gather, (self.starts, self.ends), dtype=tf.int32)
        eff_indices = tf.where(tf.not_equal(whole_links[:,0], -1))
        self.links = tf.gather(whole_links, eff_indices[:,0])


    def train_on_batch(self, X):
        feed_dict = {self.anaphors: np.reshape(X['anaphors'], [-1]), self.words: X['words'],
                     self.antecedents: np.reshape(X['antecedents'], [-1]), self.pair_antecedents: np.reshape(X['pair_antecedents'], [-1]),
                     self.pair_anaphors: np.reshape(X['pair_anaphors'], [-1]), self.pair_features: X['pair_features'],
                     self.spans: X['spans'], self.mention_features: X['mention_features'],
                     self.gold_y: np.reshape(X['y'], [-1]), self.gold_anaphoricities: np.reshape(X['anaphoricities'], [-1])}
        if self.model_props.top_pairs:
            feed_dict[self.starts] = np.reshape(X['starts'], [-1])
            feed_dict[self.ends] = np.reshape(X['ends'], [-1])
            feed_dict[self.score_inds] = np.reshape(X['score_inds'], [-1])
        elif self.model_props.ranking:
            feed_dict[self.reindex] = np.reshape(X['reindex'], [-1])
            feed_dict[self.starts] = np.reshape(X['starts'], [-1])
            feed_dict[self.ends] = np.reshape(X['ends'], [-1])
            feed_dict[self.costs] = np.reshape(X['costs'], [-1])

        loss, _ = self._sess.run([self.loss, self.train_op], feed_dict=feed_dict)
        return loss


    def predict_on_batch(self, X):
        feed_dict = {self.anaphors: np.reshape(X['anaphors'], [-1]), self.words: X['words'],
                     self.antecedents: np.reshape(X['antecedents'], [-1]), self.pair_antecedents: np.reshape(X['pair_antecedents'], [-1]),
                     self.pair_anaphors: np.reshape(X['pair_anaphors'], [-1]), self.pair_features: X['pair_features'],
                     self.spans: X['spans'], self.mention_features: X['mention_features'],
                     self.gold_y: np.reshape(X['y'], [-1]), self.gold_anaphoricities: np.reshape(X['anaphoricities'], [-1])}
        if self.model_props.top_pairs:
            feed_dict[self.starts] = np.reshape(X['starts'], [-1])
            feed_dict[self.ends] = np.reshape(X['ends'], [-1])
            feed_dict[self.score_inds] = np.reshape(X['score_inds'], [-1])
        elif self.model_props.ranking:
            feed_dict[self.reindex] = np.reshape(X['reindex'], [-1])
            feed_dict[self.starts] = np.reshape(X['starts'], [-1])
            feed_dict[self.ends] = np.reshape(X['ends'], [-1])
            feed_dict[self.costs] = np.reshape(X['costs'], [-1])
            feed_dict[self.ids] = np.reshape(X['ids'], [-1, 2])

        if self.model_props.anaphoricity_only:
            return self._sess.run([self.anaphoricities], feed_dict=feed_dict)
        else:
            if self.model_props.ranking:
                return self._sess.run([self._y, self._z], feed_dict=feed_dict)
            return self._sess.run([self.anaphoricities, self._y], feed_dict=feed_dict)


    def load_weights_from_theano(self, session, filepath=None):
        if filepath is None:
            filepath = os.path.join(directories.MODELS + self.model_props.load_weights_from,
                                    self.model_props.weights_file+".hdf5")
        f = h5py.File(filepath)
        g = f['graph']
        weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]

        for i, tf_var in enumerate(tf.trainable_variables()):
            session.run(tf_var.assign(weights[i].value))
        f.close()