import numpy as np
import directories
import timer
import os
import h5py

from custom_neural_implementations import *

class PairwisePredModel(object):
    """
    Coreference network simplified for prediction (inference).
    Reduced the number of inputs and a few gather operations in the network.
    """
    def __init__(self, session, model_props, vectors, representation=False):
        self._sess = session
        self.is_training = False

        # self.input_sizes = {k: v.shape[1] for k, v in next(X for X in dataset).iteritems() if v.ndim == 2}
        # [Note] Use fixed dim for inference
        self.mention_feat_sz = 24
        self.pair_feat_sz = 70
        self.spans_sz = 250
        self.words_sz = 8

        self.model_props = model_props
        self.representation = representation
        self.act_func = get_activation_func(self.model_props.activation)

        self._build_inputs()

        timer.start("compile")
        self._inference(vectors)

        timer.stop("compile")

        init_op = tf.global_variables_initializer()

        self.saver = tf.train.Saver([v for v in tf.trainable_variables() if v.name.startswith(model_props.mode)])
        if model_props.load_weights_from is not None:
            session.run(init_op)
            if model_props.model_from_theano:
                self.load_weights_from_theano(session)
            else:
                target_path = os.path.join(directories.MODELS+model_props.load_weights_from, model_props.weights_file+".ckpt")
                self.old_saver = tf.train.import_meta_graph(os.path.join(target_path+'.meta'))
                self.old_saver.restore(session, target_path)
                print "loaded"
                src_vars = [v for v in tf.trainable_variables() if v.name.startswith(model_props.load_weights_from)]
                dst_vars = [v for v in tf.trainable_variables() if v.name.startswith(model_props.mode)]
                for i, tf_var in enumerate(dst_vars):
                    session.run(tf_var.assign(src_vars[i]))
        else:
            print "No model loaded"
            exit(1)


    def _build_inputs(self):
        self.words = tf.placeholder(tf.int32, [None, self.words_sz], name="words")
        self.pair_antecedents = tf.placeholder(tf.int32, [None], name="pair_antecedents")
        self.pair_anaphors = tf.placeholder(tf.int32, [None], name="pair_anaphors")
        self.pair_features = tf.placeholder(tf.float32, [None, self.pair_feat_sz], name="pair_features")

        if self.model_props.use_spans:
            self.spans = tf.placeholder(tf.float32, [None, self.spans_sz], name="spans")
        if self.model_props.anaphoricity:
            self.mention_features = tf.placeholder(tf.float32, [None, self.mention_feat_sz], name="mention_features")


    def get_top_layers(self, input):
        layer_size = self.model_props.layer_sizes[0]
        assert input.get_shape()[1] == layer_size

        input = self.act_func(input)

        for i in range(len(self.model_props.layer_sizes) - 1):
            with tf.variable_scope("layer_"+str(i+1)):
                if self.is_training and self.model_props.dropout > 0:
                    dropped = tf.nn.dropout(input, 1.0-self.model_props.dropout)
                else:
                    dropped = input

                output = fully_connected(dropped, dropped.get_shape()[1], self.model_props.layer_sizes[i + 1],
                                         activation=self.act_func)
            input = output

        if not self.representation:
            with tf.variable_scope("score"):
                if self.is_training and self.model_props.dropout > 0:
                    score_drop = tf.nn.dropout(input, 1.0-self.model_props.dropout)
                else:
                    score_drop = input

                with tf.variable_scope("layer_1"):
                    ff_1 = fully_connected(score_drop, score_drop.get_shape()[1], 1)
                with tf.variable_scope("layer_2"):
                    ff_2 = fully_connected(ff_1, 1, 1, force_init=True)
                if not self.model_props.ranking:
                    ff_2 = tf.nn.sigmoid(ff_2)

        return tf.reshape(ff_2, [-1])


    def get_anaphoricity_reprs(self):
        layer_size = self.model_props.layer_sizes[0]

        with tf.variable_scope("spans"):
            anaphoricity_spans = fully_connected(self.spans, self.spans.get_shape()[1], layer_size)

        with tf.variable_scope("anaphoricity_emb"):
            anaphoricity_emb = fully_connected(self.flattend_embeddings, self.flattend_embeddings.get_shape()[1], layer_size)

        with tf.variable_scope("anaphoricity_feat"):
            anaphoricity_feat = fully_connected(self.mention_features, self.mention_features.get_shape()[1], layer_size)

        return tf.reduce_sum([anaphoricity_spans, anaphoricity_emb, anaphoricity_feat], reduction_indices=0)


    def get_mention_reprs(self, scope_name, pair_input):
        layer_size = self.model_props.layer_sizes[0]

        with tf.variable_scope(scope_name):
            with tf.variable_scope("spans"):
                mention_spans = fully_connected(self.spans, self.spans.get_shape()[1], layer_size)
                pair_mention_spans = tf.gather(mention_spans, pair_input)

            with tf.variable_scope("mention_emb"):
                pair_mention_embs = gather_and_FF(self.flattend_embeddings, pair_input, layer_size)

            return tf.reduce_sum([pair_mention_spans, pair_mention_embs], 0)


    def _inference(self, vectors):
        with tf.variable_scope("embedding"):
            self.lookup = tf.get_variable(name="lookup", shape=[vectors.shape[0], vectors.shape[1]],
                                          trainable=not self.model_props.freeze_embeddings)
        word_embeddings = tf.nn.embedding_lookup(self.lookup, self.words)
        w_shape = word_embeddings.get_shape().as_list()
        self.flattend_embeddings = tf.reshape(word_embeddings, shape=[-1, np.prod(w_shape[1:])])

        with tf.variable_scope("anaphoricity"):
            anaphoricity_reprs = self.get_anaphoricity_reprs()
            self.top_anaphoricity = self.get_top_layers(anaphoricity_reprs)

        pair_antecedent_reprs = self.get_mention_reprs('antecedent', self.pair_antecedents)
        pair_anaphor_reprs = self.get_mention_reprs('anaphor', self.pair_anaphors)

        with tf.variable_scope("pair_feature"):
            pair_feature_reprs = fully_connected(self.pair_features, self.pair_features.get_shape()[1],
                                      self.model_props.layer_sizes[0])

            self.pair_scores = self.get_top_layers(tf.reduce_sum([pair_antecedent_reprs, pair_anaphor_reprs, pair_feature_reprs],
                                                    reduction_indices=0))

        ana_score_W = tf.Variable(tf.constant(-0.3, shape=[1, 1]), name="ana_score_W", trainable=False)
        ana_score_b = tf.Variable(tf.constant(-1.0, shape=[1]), name="ana_score_b", trainable=False)
        self.anaphoricity_scores = tf.nn.xw_plus_b(tf.expand_dims(self.top_anaphoricity, 1), ana_score_W, ana_score_b)


    def predict_on_batch(self, X):
        feed_dict = {
                     self.words: X['words'],
                     self.pair_antecedents: np.reshape(X['pair_antecedents'], [-1]),
                     self.pair_anaphors: np.reshape(X['pair_anaphors'], [-1]),
                     self.pair_features: X['pair_features'],
                     self.spans: X['spans'],
                     self.mention_features: X['mention_features']}

        return self._sess.run([self.anaphoricity_scores, self.pair_scores], feed_dict=feed_dict)


    def load_weights_from_theano(self, session, filepath=None):
        if filepath is None:
            filepath = os.path.join(directories.MODELS + self.model_props.load_weights_from,
                                    self.model_props.weights_file+".hdf5")
        f = h5py.File(filepath)
        g = f['graph']
        weights = [g['param_{}'.format(p)] for p in range(g.attrs['nb_params'])]

        for i, tf_var in enumerate(tf.trainable_variables()):
            session.run(tf_var.assign(weights[i].value))
        f.close()