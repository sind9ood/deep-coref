import utils
import directories
import model_properties
from datasets import PairDataBuilder, MentionDataBuilder, DocumentDataBuilder
from word_vectors import WordVectors
from pairwise_pred_models import PairwisePredModel
from datasets import get_mention_features
from datasets import get_distance_features
import tensorflow as tf
import numpy as np


def docs_gen(dataset_name):
    for d in utils.load_json_lines(directories.RAW + dataset_name):
        yield d


def convert(data_column):
    return np.array(data_column.data, dtype='bool') \
        if data_column.name == 'y' or data_column.name == 'pf' else np.vstack(data_column.data)


def get_dense_features_anaphoricity(mention_features, document_features, doc_size, model_props):
    fs = np.hstack(get_mention_features(mention_features, doc_size, model_props))
    tiled = np.tile(document_features, (fs.shape[0], 1))
    return np.concatenate((fs, tiled), axis=1)


def get_dense_features(m1_features, m2_features, pair_features, document_features, doc_size,
                       model_props):

    fs = np.hstack([pair_features] +
                   (get_distance_features(m1_features, m2_features)
                    if model_props.use_distance else []) +
                   get_mention_features(m1_features, doc_size, model_props) +
                   get_mention_features(m2_features, doc_size, model_props))
    tiled = np.tile(document_features, (fs.shape[0], 1))
    return np.concatenate((fs, tiled), axis=1)


def get_doc_vectors(doc_name, vectors):
    """
    Assume we have only 1 sample doc for prediction.
    """
    print "Building document vectors."
    doc_vectors = []
    for d in docs_gen(doc_name):
        sentences = {}
        for mention_num in sorted(d["mentions"].keys(), key=int):
            m = d["mentions"][mention_num]
            # did = m["doc_id"]
            if m['sent_num'] not in sentences:
                sentences[m['sent_num']] = m['sentence']

        v = np.zeros(vectors.vectors[0].size)
        n = 0
        for s in sentences.values():
            for w in s:
                v += vectors.vectors[vectors[w]]
                n += 1
        doc_vectors.append (v / n)
    return doc_vectors


def get_data_builders(doc_name, vectors, doc_vectors, sent_num):
    print "Building dataset."
    pairsDB = PairDataBuilder(None)
    mentionsDB = MentionDataBuilder(None)
    docsDB = DocumentDataBuilder(None)

    for d in docs_gen(doc_name):
        ms, ps = mentionsDB.size(), pairsDB.size()
        mention_positions = {}
        for mention_num in sorted(d["mentions"].keys(), key=int):
            if d["mentions"][mention_num]["sent_num"] < sent_num:
                mention_positions[mention_num] = mentionsDB.size()
                mentionsDB.add_mention(d["mentions"][mention_num], vectors,
                                       doc_vectors[d["mentions"][mention_num]["doc_id"]])

        for key in sorted(d["labels"].keys(), key=lambda k: (int(k.split()[1]), int(k.split()[0]))):
            k1, k2 = key.split()
            if k1 in mention_positions and k2 in mention_positions:
                pairsDB.add_pair(d["labels"][key], mention_positions[k1], mention_positions[k2],
                                 int(d["mentions"][k1]["doc_id"]),
                                 int(d["mentions"][k1]["mention_id"]),
                                 int(d["mentions"][k2]["mention_id"]),
                                 d["pair_features"][key])

        me, pe = mentionsDB.size(), pairsDB.size()
        docsDB.add_doc(ms, me, ps, pe, d["document_features"])
        break

    return docsDB, mentionsDB, pairsDB


def get_dataset(data_builder):
    dDB, mDB, pDB = data_builder

    mention_set = {}
    mention_set['dids'] = convert(mDB.dids)
    mention_set['features'] = convert(mDB.features)
    mention_set['mention_ids'] = convert(mDB.mention_ids)
    mention_set['mention_nums'] = convert(mDB.mention_nums)
    mention_set['spans'] = convert(mDB.spans)
    mention_set['words'] = convert(mDB.words)

    pair_set = {}
    pair_set['pair_features'] = convert(pDB.pair_features)
    pair_set['pair_ids'] = convert(pDB.pair_ids)
    pair_set['pair_indices'] = convert(pDB.pair_indices)
    pair_set['y'] = convert(pDB.y)

    doc_set = {}
    doc_set['features'] = convert(dDB.features)
    doc_set['mention_inds'] = convert(dDB.mention_inds)
    doc_set['pair_inds'] = convert(dDB.pair_inds)

    return doc_set, mention_set, pair_set


def prepare_env(sess, wv):
    ranker_props = model_properties.MentionRankingProps(name="ranking",
                                                        load_weights_from="reward_rescaling",
                                                        weights_file="best_weights",
                                                        model_from_theano=True,
                                                        use_doc_embedding=False)

    with tf.variable_scope("ranking"):
        ranker = PairwisePredModel(sess, ranker_props, wv.vectors)

    return ranker, ranker_props


class Pred_Batched_Dataset():

    def __init__(self, model_props, dataset):

        self.model_props = model_props
        self.words = dataset[0]['words']
        if not model_props.use_dep_reln:
            self.words = self.words[:, :-1]
        if model_props.use_spans:
            self.spans = dataset[0]['spans']
            if not model_props.use_doc_embedding:
                self.spans[:, -self.spans.shape[1] / 5:] = 0
        self.mention_features = dataset[0]['features']
        self.document_features = dataset[2]['features']
        if not model_props.use_genre:
            self.document_features = np.zeros((self.document_features.shape[0], 1))

        self.pair_features = dataset[1]['pair_features'][:, model_props.active_pair_features]
        self.mention_ids = dataset[0]['mention_ids']

        doc_pairs = dataset[2]['pair_inds']
        doc_mentions = dataset[2]['mention_inds']

        self.pair_nums = []
        for did in np.arange(doc_pairs.shape[0]):
            ms, me = doc_mentions[did]
            self.pair_antecedents = np.concatenate([np.arange(ana)
                                               for ana in range(0, me - ms)])
            self.pair_anaphors = np.concatenate([ana *
                                            np.ones(ana, dtype='int32')
                                            for ana in range(0, me - ms)])
            self.pair_nums += [np.array(p) for p in zip(self.pair_antecedents, self.pair_anaphors)]
        self.pair_nums = np.vstack(self.pair_nums)


    def __iter__(self):
        X = {}
        X['words'] = self.words
        X['spans'] = self.spans
        X['mention_features'] = get_dense_features_anaphoricity(
            self.mention_features, self.document_features, len(self.mention_ids), self.model_props)
        X['pair_antecedents'] = self.pair_antecedents
        X['pair_anaphors'] = self.pair_anaphors
        X['pair_features'] = get_dense_features(
                self.mention_features[self.pair_antecedents],
                self.mention_features[self.pair_anaphors],
                self.pair_features, self.document_features, len(self.mention_ids),self.model_props)

        yield X


def inst_predict(target_doc, ranker, ranker_props, wv):
    doc_vectors = get_doc_vectors(target_doc, wv)

    # Build batch dataset
    docs_DB, mentions_DB, pairs_DB = get_data_builders("sample", wv, doc_vectors, 1)
    doc_set, mention_set, pair_set = get_dataset([docs_DB, mentions_DB, pairs_DB])
    sample_batch_dataset = Pred_Batched_Dataset(ranker_props, dataset=[mention_set, pair_set, doc_set])

    for X in sample_batch_dataset:
        return ranker.predict_on_batch(X)


if __name__ == '__main__':
    with tf.Session() as sess:
        wv = WordVectors(load=True)
        ranker, ranker_props = prepare_env(sess, wv)
        pairs = inst_predict("sample", ranker, ranker_props, wv)
        print pairs