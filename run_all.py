import directories
import build_datasets
import preprocessing
import pairwise_learning
import model_properties
import document
import os, sys


def setup():
    preprocessing.main()
    build_datasets.build_datasets(reduced=True)
    build_datasets.build_datasets(reduced=False)
    document.main()

def already_trained(name, weights):
    return os.path.exists(directories.MODELS + name + '/' + weights + '.ckpt')

def pretrain(model_props, device):
    if not already_trained('all_pairs', 'weights_140'):
        model_props.set_name('all_pairs')
        model_props.set_mode('all_pairs')
        model_props.load_weights_from = None
        pairwise_learning.train_tf(model_props, device, n_epochs=150)

    if not already_trained('top_pairs', 'weights_40'):
        model_props.set_name('top_pairs')
        model_props.set_mode('top_pairs')
        model_props.load_weights_from = 'all_pairs'
        model_props.weights_file = 'weights_140'
        pairwise_learning.train_tf(model_props, device, n_epochs=50)

def make_predictions(model_props, load_weights_from, datasets, device='cpu:0', save_scores=False):
    model_props.load_weights_from = load_weights_from
    model_props.weights_file = 'final_weights'
    for i, dataset_name in enumerate(datasets):
        reuse = False if i == 0 else True
        pairwise_learning.test(model_props=model_props, save_scores=save_scores, save_output=True,
                               device=device, dataset_name=dataset_name, reuse=reuse)

def train_pairwise(model_props, device, mode='ranking'):
    if not model_props.model_from_theano:
        pretrain(model_props, device)
    model_props.set_name(mode)
    model_props.set_mode(mode)
    model_props.load_weights_from = 'top_pairs'
    model_props.weights_file = 'weights_40'
    pairwise_learning.train_tf(model_props, device, n_epochs=100)

def train_and_test_pairwise(model_props, mode='ranking', device='cpu:0'):
    train_pairwise(model_props, device, mode=mode)
    model_props.set_name(mode)
    make_predictions(model_props, mode, ["dev", "test"], device)

def train_best_model(dev="cpu:0"):
    train_and_test_pairwise(model_properties.MentionRankingProps(use_doc_embedding=True), mode='reward_rescaling', device=dev)

if __name__ == '__main__':
    # setup()
    train_best_model()
