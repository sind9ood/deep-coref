import numpy as np
import tensorflow as tf
import theano.tensor as T
import theano
import numpy.testing as npt


def test_theano_nonzero_func():

    # Test theano version
    csts = [[1.2], [0.0], [0.8], [2.5], [0.5], [0.0]]
    print (T.eq(csts, 0).nonzero()[0]).eval()

    # Test tensorflow version
    with tf.Session() as sess:
        csts_T = tf.constant(csts)
        ind = tf.where(tf.equal(csts_T, 0))[:,0]
        print sess.run(ind)


def test_gather_max():
    '''
    'imax' mode in theano version
    '''
    starts = tf.constant([0, 3, 5], dtype=tf.int32)
    ends = tf.constant([1, 5, 8], dtype=tf.int32)
    reordered = tf.constant([2.3, 1.2, 5.3, 2.7, 0.9, 1.5, 0.7, 2.2, 2.7, 1.6])

    def gather_max(r):
        return tf.reduce_max(tf.gather(reordered, tf.range(r[0], r[1])))

    with tf.Session() as sess:
        _y = tf.map_fn(gather_max, (starts, ends), dtype=tf.float32)
        result = sess.run(_y)

    print result
    npt.assert_array_almost_equal([2.30, 2.70, 2.20], result, decimal=5)


def test_risk_mode():

    # Test theano version
    startsT = T.imatrix("starts")
    endsT = T.imatrix("ends")
    scoresT = T.fmatrix("scores")
    costsT = T.fmatrix("costs")

    fn = lambda start, end, scs, csts: T.sum(csts[start[0]:end[0]] * T.nnet.softmax(scs[start[0]:end[0]].T).T)
    outputs, _ = theano.scan(fn=fn,
                             sequences=[startsT, endsT],
                             non_sequences=[scoresT, costsT])

    reshaped = outputs.reshape((outputs.size, 1))

    f_risk = theano.function(inputs=[scoresT, costsT, startsT, endsT], outputs=reshaped)

    scores = np.array([[2.3], [1.2], [5.3], [2.7], [0.9], [1.5], [0.7], [2.2], [2.7], [1.6]], dtype='float32')
    costs = np.array([[0.4], [1.0], [0.0], [0.8], [0.9], [1.0], [0.7], [1.0], [0.4], [1.0]], dtype='float32')
    starts = np.array([[0], [3],[5]], dtype='int32')
    ends = np.array([[1], [5], [8]], dtype='int32')

    theano_result = f_risk(scores, costs, starts, ends)

    # Test tensorflow version
    def merge_risk(scores, costs, starts, ends):
        scores = tf.reshape(scores, [-1])
        costs = tf.reshape(costs, [-1])
        starts = tf.reshape(starts, [-1])
        ends = tf.reshape(ends, [-1])

        def gather_max_with_softmax(r):
            gather_score = tf.gather(scores, tf.range(r[0], r[1]))
            gather_cost = tf.gather(costs, tf.range(r[0], r[1]))
            return tf.reduce_sum(gather_cost * tf.nn.softmax(gather_score))

        return tf.reshape(tf.map_fn(gather_max_with_softmax, (starts, ends), dtype=tf.float32), [-1, 1])

    with tf.Session() as sess:
        tensorflow_result = sess.run(merge_risk(scores, costs, starts, ends))

    print theano_result
    print tensorflow_result
    npt.assert_array_almost_equal(theano_result, tensorflow_result, decimal=5)


def test_max_margin():

    # Test theano version
    startsT = T.imatrix("starts")
    endsT = T.imatrix("ends")
    scoresT = T.fmatrix("scores")
    costsT = T.fmatrix("costs")

    fn = lambda start, end, scs, csts: \
        T.max(csts[start[0]:end[0]] *
              (3 + scs[start[0]:end[0]] -
               T.max(scs[start[0]:end[0]][T.eq(csts[start[0]:end[0]], 0).nonzero()[0]])))

    outputs, _ = theano.scan(fn=fn,
                             outputs_info=None,
                             sequences=[startsT, endsT],
                             non_sequences=[scoresT, costsT])
    reshaped = outputs.reshape((outputs.size, 1))

    f_max_margin = theano.function(inputs=[scoresT, costsT, startsT, endsT], outputs=reshaped)

    scores = np.array([[2.3], [1.2], [5.3], [2.7], [0.9], [1.5], [0.7], [2.2], [2.7], [1.6]], dtype='float32')
    costs = np.array([[0.4], [0.0], [0.0], [0.8], [0.0], [0.0], [0.7], [1.0], [0.0], [0.4]], dtype='float32')
    starts = np.array([[0], [3], [5]], dtype='int32')
    ends = np.array([[2], [5], [9]], dtype='int32')

    theano_result = f_max_margin(scores, costs, starts, ends)

    # Test tensorflow version
    def merge_max_margin(scores, costs, starts, ends):
        starts = tf.reshape(starts, [-1])
        ends = tf.reshape(ends, [-1])
        costs = tf.reshape(costs, [-1])
        scores = tf.reshape(scores, [-1])

        def gather_max(r):
            cst = tf.gather(costs, tf.range(r[0], r[1]))
            scr = tf.gather(scores, tf.range(r[0], r[1]))
            ind = tf.squeeze((tf.where(tf.equal(cst, 0))))
            score_ind = tf.gather(scr, ind)
            return tf.reduce_max(cst * (3.0 + scr - tf.reduce_max(score_ind)))

        return tf.reshape(tf.map_fn(gather_max, (starts, ends), dtype=tf.float32), [-1, 1])

    with tf.Session() as sess:
        tensorflow_result = sess.run(merge_max_margin(scores, costs, starts, ends))

    print theano_result
    print tensorflow_result
    npt.assert_array_almost_equal(theano_result, tensorflow_result, decimal=5)


def test_imaxavg():
    '''
    "merge_max_mean" in tensorflow version
    '''

    # Test theano version
    startsT = T.imatrix("starts")
    endsT = T.imatrix("ends")
    x = T.matrix("x", dtype='float32')

    fn = lambda start, end, x: T.concatenate([T.max(x[start[0]:end[0]], axis=0),
                                              T.mean(x[start[0]:end[0]], axis=0)])
    outputs, _ = theano.scan(fn=fn,
                             outputs_info=None,
                             sequences=[startsT, endsT],
                             non_sequences=x)

    f_imaxavg = theano.function(inputs=[startsT, endsT, x], outputs=outputs)

    data = np.array([[2.3], [1.2], [5.3], [2.7], [0.9], [1.5], [0.7], [2.2], [2.7], [1.6]], dtype='float32')
    starts = np.array([[0], [3], [5]], dtype='int32')
    ends = np.array([[2], [5], [7]], dtype='int32')

    theano_result = f_imaxavg(starts, ends, data)

    '''
    Dim for data should be 2, while dim for indexing vectors remains 1
    '''
    def merge_imaxavg(data, starts, ends):
        starts = tf.reshape(starts, [-1])
        ends = tf.reshape(ends, [-1])

        def gather_max_mean(r):
            gathered = tf.gather(data, tf.range(r[0], r[1]))
            max_gather = tf.reduce_max(gathered, reduction_indices=0)
            mean_gather = tf.reduce_mean(gathered, reduction_indices=0)
            return tf.concat(0, [max_gather, mean_gather])

        return tf.map_fn(gather_max_mean, (starts, ends), dtype=tf.float32)

    with tf.Session() as sess:
        tensorflow_result = sess.run(merge_imaxavg(data, starts, ends))

    print theano_result
    print tensorflow_result
    npt.assert_array_almost_equal(theano_result, tensorflow_result, decimal=5)


def test_summed_cross_entropy():
    # Test theano version
    def summed_cross_entropy(y_true, y_pred, n):
        epsilon = 1.0e-7
        y_pred = T.clip(y_pred, epsilon, 1.0 - epsilon)
        bce = T.nnet.binary_crossentropy(y_pred, y_true).sum()
        return bce / n

    y_true = T.ivector("True Y")
    y_pred = T.fvector("Pred Y")
    xent = summed_cross_entropy(y_true, y_pred, 0.1)

    f_xent = theano.function(inputs=[y_true, y_pred], outputs=xent)

    y_t = np.array([1, 0, 0], dtype='int32')
    y_p = np.array([0.87877, 0.02512, 0.01121], dtype='float32')

    theano_result = f_xent(y_t, y_p)

    # Test tensorflow version
    def get_summed_xent(scale_factor, y_true, y_pred):
        epsilon = 1.0e-7
        y_pred = tf.clip_by_value(y_pred, epsilon, 1.0 - epsilon)
        y_true = tf.to_float(y_true)
        bce = -tf.reduce_sum(y_true * tf.log(y_pred) + (1.0-y_true) * tf.log(1.0-y_pred))

        return bce / scale_factor

    with tf.Session() as sess:
        tensorflow_result = sess.run(get_summed_xent(0.1, y_t, y_p))

    print theano_result
    print tensorflow_result
    npt.assert_almost_equal(theano_result, tensorflow_result, 5)


if __name__ == '__main__':
    print "Testing gather_max func.\n"
    test_gather_max()
    print "\nTesting risk mode.\n"
    test_risk_mode()
    print "\nTesting max margin mode.\n"
    test_max_margin()
    print "\nTesting imaxavg mode.\n"
    test_imaxavg()
    print "\nTesting summed cross entropy.\n"
    test_summed_cross_entropy()